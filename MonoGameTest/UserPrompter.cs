namespace MonoGameTest {
    public static class UserPrompter {
        public static bool confirmOkToExitGame(bool do_confirm_flag = true) {
            if (!do_confirm_flag) {
                return true;
            }
            const string MESSAGE_STR = "Are you sure?";
            return System.Windows.Forms.MessageBox.Show(
                MESSAGE_STR,
                MESSAGE_STR,
                System.Windows.Forms.MessageBoxButtons.YesNo)
                   == System.Windows.Forms.DialogResult.Yes;
        }

        public static void showExitingMessage(bool show_message_flag = true) {
            if (!show_message_flag) {
                return;
            }
            const string MESSAGE_STR = "Thank you for playing!";
            System.Windows.Forms.MessageBox.Show(
                MESSAGE_STR,
                MESSAGE_STR,
                System.Windows.Forms.MessageBoxButtons.OK);
        }
    }
}