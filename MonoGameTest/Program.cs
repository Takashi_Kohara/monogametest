﻿namespace MonoGameTest {
#if WINDOWS || LINUX
    public static class Program {
        // NOTE: setting [STAThread] attribute sometime cause application not finish problem.
        //[STAThread] // comment out
        static void Main() {
            using (var game = new GameMain()) {
                game.Run();
            }
        }
    }
#endif
}