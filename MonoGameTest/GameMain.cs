﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace MonoGameTest {
    public class GameMain: Game {
        private const float BUFFER_WIDTH = 1280;
        private const float BUFFER_HEIGHT = 720;

        // [start] per-application constants
        // [end] per-application constants

        GraphicsDeviceManager _graphics;
        SpriteBatch _sprite_batch;

        private int _frame_count;

        // [start] per-application variables
        private Texture2D _texture;
        private SoundEffect _sound_effect;
        private SoundEffectInstance _sound_effect_instance;
        private Song _song;
        private SpriteFont _sprite_font;

        private Vector2 _position;
        private Vector2 _velocity;
        private float _rotation;
        private string _touch_info_str;
        // [end] per-application variables

        public GameMain() {
            _graphics = new GraphicsDeviceManager(this) {
                PreferredBackBufferWidth = (int) BUFFER_WIDTH,
                PreferredBackBufferHeight = (int) BUFFER_HEIGHT
            };

            Content.RootDirectory = "Content";

            Exiting += eventGameExiting;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            // Override close button
            // http://gamedev.stackexchange.com/questions/30957/override-close-button
            var form = System.Windows.Forms.Control.FromHandle(Window.Handle) as System.Windows.Forms.Form;
            if (form != null) {
                form.FormClosing += eventFormClosing;
            }

            // [start] per-application initialization logic
            _position.X = BUFFER_WIDTH / 2;
            _position.Y = BUFFER_HEIGHT / 2;
            _velocity.X = 10f;
            _velocity.Y = 10f;
            // [end] per-application initialization logic

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            _sprite_batch = new SpriteBatch(GraphicsDevice);

            // [start] use this.Content to load per-application game content
            // font
            _sprite_font = Content.Load<SpriteFont>("sprite_font");
            // texture
            _texture = Content.Load<Texture2D>("kobun");
            // sound effect
            _sound_effect = Content.Load<SoundEffect>("sound");
            _sound_effect_instance = _sound_effect.CreateInstance();
            // song
            _song = Content.Load<Song>("song");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(_song);
            // [end] use this.Content to load per-application game content
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent() {
            // [start] Unload any non ContentManager content
            Content.Unload();
            // [end] Unload any non ContentManager content
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="game_time">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime game_time) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                if (UserPrompter.confirmOkToExitGame(false)) {
                    Exit();
                    return;
                }
            }

            // [start] per-application update logic
            // play sound effect instance
            if (_frame_count == 0) {
                _sound_effect_instance.IsLooped = true;
                _sound_effect_instance.Volume = 1.0f;
                _sound_effect_instance.Pitch = 0.0f;
                _sound_effect_instance.Play();
            }

            // key input
            if (Keyboard.GetState().IsKeyDown(Keys.Left)) {
                _velocity.X -= 1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right)) {
                _velocity.X += 1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Up)) {
                _velocity.Y -= 1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down)) {
                _velocity.Y += 1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter)) {
                _position.X = BUFFER_WIDTH / 2;
                _position.Y = BUFFER_HEIGHT / 2;
                _velocity.X = 10f;
                _velocity.Y = 10f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space)) {
                _position.X = BUFFER_WIDTH / 2;
                _position.Y = BUFFER_HEIGHT / 2;
                _velocity.X = 0f;
                _velocity.Y = 0f;
            }

            // game pad input
            var game_pad_state = GamePad.GetState(PlayerIndex.One);
            _position.X += game_pad_state.ThumbSticks.Left.X * 10;
            _position.Y += -game_pad_state.ThumbSticks.Left.Y * 10;
            _velocity.X += game_pad_state.ThumbSticks.Right.X;
            _velocity.Y += -game_pad_state.ThumbSticks.Right.Y;
            _rotation -= MathHelper.Pi / 60 * game_pad_state.Triggers.Left;
            _rotation += MathHelper.Pi / 60 * game_pad_state.Triggers.Right;
            if (game_pad_state.Buttons.A == ButtonState.Pressed) {
                _position.X = BUFFER_WIDTH / 2;
                _position.Y = BUFFER_HEIGHT / 2;
                _velocity.X = 10f;
                _velocity.Y = 10f;
            }
            if (game_pad_state.Buttons.B == ButtonState.Pressed) {
                _position.X = BUFFER_WIDTH / 2;
                _position.Y = BUFFER_HEIGHT / 2;
                _velocity.X = 0f;
                _velocity.Y = 0f;
            }

            // touch input
            _touch_info_str = "";
            var touch_count = 0;
            foreach (var state in TouchPanel.GetState()) {
                ++touch_count;
                _touch_info_str +=
                    $"{touch_count} ID={state.Id} X={state.Position.X} Y={state.Position.Y} STATE={state.State}\n";
            }

            // add velocity
            _position += _velocity;

            // reflect at screen buffer edge
            if (_position.X <= 0) {
                _velocity.X = System.Math.Abs(_velocity.X);
            }
            if (_position.X >= BUFFER_WIDTH) {
                _velocity.X = -System.Math.Abs(_velocity.X);
            }
            if (_position.Y <= 0) {
                _velocity.Y = System.Math.Abs(_velocity.Y);
            }
            if (_position.Y >= BUFFER_HEIGHT) {
                _velocity.Y = -System.Math.Abs(_velocity.Y);
            }
            // [end] per-application update logic

            base.Update(game_time);

            ++_frame_count;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="game_time">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime game_time) {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // [start] per-application drawing code
            _sprite_batch.Begin();
            var sound_position = MediaPlayer.PlayPosition;
            _sprite_batch.Draw(_texture,
                position: _position,
                origin: _texture.Bounds.Size.ToVector2() / 2,
                color: Color.White,
                rotation: _rotation);
            _sprite_batch.DrawString(_sprite_font, "[song] " + sound_position, new Vector2(10, 10), Color.White);
            _sprite_batch.DrawString(_sprite_font, "[touch info]\n" + _touch_info_str, new Vector2(10, 30), Color.White);
            _sprite_batch.End();
            // [end] per-application drawing code

            base.Draw(game_time);
        }

        private void eventGameExiting(object sender, System.EventArgs e) {
            UserPrompter.showExitingMessage(false);
        }

        private void eventFormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e) {
            if (!UserPrompter.confirmOkToExitGame(false)) {
                e.Cancel = true;
            }
        }
    }
}